<?php 

namespace MeForms;


function &array_setdefault(&$arr, $key, $dflt) {
    if (!array_key_exists($key, $arr)) {
        $arr[$key] = $dflt;
    }
    return $arr[$key];
}


/* Used to stop validation */
class ValidationError extends \Exception {}


interface IValidator {
    function __invoke($value);
}


class RequiredValidator implements IValidator {
    public function __construct($check_against = null) {
        $this->check_against = $check_against;
    }

    public function __invoke($value) {
        $errors = [];
        
        if ($value == null or 
            $value == $this->check_against or 
            (string)($value) === '' or
            (is_array($value) and empty($value))) {
            throw new ValidationError('Dieses Feld darf nicht leer sein.');
        }
        return $errors;
    }
}


class StrLenValidator implements IValidator {
    public function __construct($minLen = -1, $maxLen = -1) {
        $this->minLen = $minLen;
        $this->maxLen = $maxLen;
    }

    public function __invoke($value) {
        $errors = [];
        if ($this->minLen !== -1) {
            if (mb_strlen($value, 'utf-8') < $this->minLen) {
                array_push($errors, "Die Eingabe muss min. $this->minLen Zeichen lang sein.");
            }
        }
        
        if ($this->maxLen !== -1) {
            if (mb_strlen($value, 'utf-8') > $this->maxLen) {
                array_push($errors, "Die Eingabe darf max. $this->maxLen Zeichen lang sein.");
            }
        }

        return $errors;
    }
}


class EmailValidator implements IValidator {
    public function __invoke($value) {
        /* use built-in email validation */
        if (!(filter_var($value, FILTER_VALIDATE_EMAIL))){
            $msg = "E-Mail Adresse ist ungültig.";
            return [$msg];
        }
        return [];
    }
}


class PhoneValidator implements IValidator {
    public $RE = "<^((\\+|00)[1-9]\\d{0,3}|0 ?[1-9]|\\(00? ?[1-9][\\d ]*\\))[\\d\\-/ ]*$>";
    
    public function __invoke($value) {
        $error_msgs = [];
        
        if (!(preg_match($this->RE, $value))) {
            array_push($error_msgs, "Telefonnummer ist ungültig.");
        }

        return $error_msgs;
    }
}


class SelectValidator implements IValidator {
    public function __construct($options) {
        $this->options = $options;
    }

    public function __invoke($values) {
        if (!isset($values)) {
            return [];
        }

        $optvals = array_keys($this->options);

        foreach($values as $val) {
            if (!in_array($val, $optvals)) {
                return ["Die Eingabe " . ($val) . " ist nicht erlaubt."];
            }
        }
        return [];
    }
}


class AntiBotValidator implements IValidator {
    public function __invoke($value) {
        $error_msgs = [];
        if ($value != null and mb_strlen($value, 'utf-8') > 0) {
            array_push($error_msgs, "No bots, please!");
        }
        return $error_msgs;
    }
}


class AntiCSRFValidator implements IValidator {
    public function __invoke($value) {
        if (isset($_COOKIE[$this->cookie_name]) && 
            $_COOKIE[$this->cookie_name] != $value) {
            throw new ValidationError('Cross Site Request Forgery detected');
        }
        return [];
    }
}


class FieldBase {
    public $name_on_form;
    public $validators;
    public $required;
    
    private $defaults;
    private $_value;
    
    
    function __construct($name_on_form, $validators = [], $defaults = null) {
        $this->name_on_form = $name_on_form;
        $this->required = false;
        $this->defaults = $defaults;
        $vtors = [];
        foreach($validators as $vtor) {
            if ($vtor instanceof RequiredValidator) {
                array_splice($vtors, 0, 0, [$vtor]);
                $this->required = true;
            }
            else {
                array_push($vtors, $vtor);
            }
        }
        $this->validators = $vtors;
        $_value = "";
    }

    public function has_defaults() {
        return isset($this->defaults);
    }

    public function get_defaults() {
        return $this->defaults;
    }
    
    public function validate() {
        $errors = [];
        foreach($this->validators as $vtor) {
            array_splice($errors, count($errors), 0, $vtor($this->get()));
        }
        return $errors;
    }
    
    public function get() {
        return $this->_value;
    }

    public function set($val) {
        $this->_value = $val;
    }
}


class Field extends FieldBase {
    function __construct($name_on_form, $validators = []) {
        parent::__construct($name_on_form, $validators);
    }
}


class EmailField extends FieldBase {
    function __construct($name_on_form, $validators = [], $defaults = null) {
        $validators = array_merge([new EmailValidator()], $validators);
	parent::__construct($name_on_form, $validators, $defaults);
    }
}


class PhoneField extends FieldBase {
    function __construct($name_on_form, $validators = [], $defaults = null) {
        $validators = array_merge([new PhoneValidator()], $validators);
        parent::__construct($name_on_form, $validators, $defaults);
    }
}


class AntiBotField extends FieldBase {
    function __construct($name_on_form) {
        $validators = [new AntiBotValidator()];
        parent::__construct($name_on_form, $validators);
    }
}


class AntiCSRFField extends FieldBase {
    function __construct($name_on_form) {
        $validators = [new AntiCSRFValidator()];
        parent::__construct($name_on_form, $validators);
    }
}


class SelectField extends FieldBase {
    function __construct($name_on_form, $options, $validators = [], $defaults = null) {
        $this->options = $options;
        $validators = array_merge([new SelectValidator($options)], $validators);
        parent::__construct($name_on_form, $validators, $defaults);
    }
}


abstract class Form {
    abstract function get_fields_def();

    function __construct($data = null) {
        if (is_null($data)) {
            $this->initiate_with_defaults();
        }

        $this->set_data($data);
        $this->errors = [];
    }

    function get($name) {
        return $this->get_field($name)->get();
    }
    
    function set($name, $value) {
        $this->set_field($name, $value);
    }

    function initiate_with_defaults() {
        foreach($this->get_fields() as $key => $field) {
            if ($field->has_defaults()) {
                $field->set($field->get_defaults());
            }
        }
    }

    function get_msg($fieldname) {
        $errs = $this->get_field_errors($fieldname);
        $field = $this->get_field($fieldname);

        return (!empty($errs) 
                   ? join(', ', $errs) 
                   : (($field->required) && !($this->get($fieldname))
                       ? 'Bitte ausfüllen' 
                       : ''));
    }

    function set_data($data) {
        $this->data = [];

       	foreach($this->get_fields() as $key => $field) {
            if (array_key_exists($field->name_on_form, $data)) {
                $field->set($data[$field->name_on_form]);
                $this->data[$field->name_on_form] = $data[$field->name_on_form];
            }
        }
    }
    
    function get_data() {
        $data = [];
        foreach($this->get_fields() as $key => $field) {
            $data[$field->name_on_form] = $field->get();
        }
        return $data;
    }
    
    private $_fields;
    function get_fields() {
        if (!isset($this->_fields)) {
            $this->_fields = $this->get_fields_def();
        }
        return $this->_fields;
    }
    
    function get_field($name) {
        return $this->get_fields()[$name];
    }
    
    function set_field($name, $value) {
        $fields = $this->get_fields();
        $fields[$name].set($value);
    }
    
    function get_class_vars() {
        return get_class_vars(get_class($this));
    }
    
    function get_cleaning_method_names() {
        $meths = [];
        foreach(get_class_methods($this) as $k => $meth_name) {
            if (substr($meth_name, 0, 5) === "clean") {
                array_push($meths, $meth_name);
            }
        }
        return $meths;
    }

    function has_errors() {
        return isset($this->errors) and !empty($this->errors);
    }

    function get_field_errors($fieldname) {
        return $this->errors[$fieldname];
    }

    function get_checked($fieldname) {
        $rv = $this->get($fieldname);
        return $rv;
    }
    
    function is_valid() {
        $this->errors = [];
        $this->cleaned_data = [];
        
        $fields = $this->get_fields();

        foreach($fields as $key => $field) {
            try {
                $errs = $field->validate();
                if (count($errs) == 0) {
                    $this->cleaned_data[$key] = $field->get();
                }
                else {
                    $this->errors[$key] = $errs;
                        }
                }
            catch (ValidationError $e) {
                array_push(array_setdefault($this->errors, $field->name_on_form, []), $e->getMessage());
            }
        }

        try {
            $cleaning_method_names = $this->get_cleaning_method_names();

            foreach($fields as $key => $field) {
                foreach($cleaning_method_names as $meth_name){
                    if ($meth_name === "clean_" . $key) {
                        $this->{$meth_name}();
                    }
                }
            }
        }
        catch (ValidationError $e) {
            array_push(array_setdefault($this->errors, $field->name_on_form, []), $e->getMessage());
            return false;
        }
        
        try {
            $index = array_search("clean", $cleaning_method_names);
            
            if ($index !== false) {
                $this->clean();
            }
        }
        catch (ValidationError $e) {
            array_push(array_setdefault($this->errors, "__global__", []), $e->getMessage());
            return false;
        }

        return count($this->errors) === 0;
    }
}

?>
