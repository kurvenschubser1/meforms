# MeForms
A PHP forms validation library, heavily inspired on the Django forms.

## Usage
```
<?php

require_once("meforms.php");


class TestForm extends MeForms\Form {
    /* 
     override the abstract function get_fields_def to provide the input
     fields to be validated.
     Return array of name => field pairs. The fields can have validator objects.
     Please consult the sources to see available ones.
    */
    function get_fields_def() {
        return [
            'tel' => new MeForms\Field('tel', [new MeForms\PhoneValidator()]),
            'honeypot' => new MeForms\AntiBotField('honeypot'),
            'csrf' => new MeForms\AntiCSRFField('csrf')
        ];
    }

    /*
     optionally provide a method named "clean_[field name]"  which will be 
     called automagically after the validators on the fields have returned.
    */
    function clean_tel() {
        array_push(MeForms\array_setdefault(
            $this->errors, "foofield", []), "bar"
        );
        array_push(MeForms\array_setdefault(
            $this->errors, "foofield", []), "baz"
        );
    }

    /*
     optionally provide a method named "clean"  which will be called 
     automagically after all the clean_[field name] validators (if any) have
     returned.
    */
    function clean() {
        if ($this->get("honeypot")) {
            throw new MeForms\ValidationError("No bots allowed!");
        }
    }
}


$data = ['tel' => '+49 176 93213700', 'honeypot' => "x"];
$form = new TestForm($data);

echo "Is valid: " . json_encode($form->is_valid()) . "\n";
echo "Errors: " . json_encode($form->errors) . "\n";
echo "Cleaned data: " . json_encode($form->cleaned_data) . "\n";
```