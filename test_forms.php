<?php

require_once("meforms.php");


class TestForm extends MeForms\Form {
    function get_fields_def() {
        return [
            'tel' => new MeForms\Field('tel', [new MeForms\PhoneValidator()]),
            'honeypot' => new MeForms\AntiBotField('honeypot'),
            'csrf' => new MeForms\AntiCSRFField('csrf')
        ];
    }
    
    function clean() {
        if ($this->get("honeypot")) {
            throw new MeForms\ValidationError("No bots allowed!");
        }
    }

    function clean_tel() {
        array_push(MeForms\array_setdefault($this->errors, "funky", []), "doodoo");
        array_push(MeForms\array_setdefault($this->errors, "funky", []), "butter");
    }
}


$data = ['tel' => '+49 176 93213700', 'honeypot' => "x"];

$form = new TestForm($data);

/*
echo "Is valid: " . json_encode($form->is_valid()) . "\n";
echo "Errors: " . json_encode($form->errors) . "\n";
echo "Cleaned data: " . json_encode($form->cleaned_data) . "\n";
*/

class NewsletterSignUpForm extends MeForms\Form {
    function get_fields_def() {
        $nl_choices = ['courses' => 'Kurse', 'expositions' => 'Ausstellungen'];
        $preset_nl_choices = ['courses', 'expositions'];
        return [
            'firstname' => new MeForms\Field('firstname', [new MeForms\RequiredValidator()]),
            'lastname' => new MeForms\Field('lastname', [new MeForms\RequiredValidator()]),
            'email' => new MeForms\EmailField('email', [new MeForms\RequiredValidator()]),
            'newsletter_choices' => new MeForms\SelectField(
				'newsletter_choices', 
				$nl_choices, 
				[new MeForms\RequiredValidator()],
				$preset_nl_choices
             ),
            'honeypot' => new MeForms\AntiBotField('tellusaboutyourself'),
            'anticsrf' => new MeForms\AntiCSRFField('anticsrf')
        ];
    }
}


$form = new NewsletterSignUpForm();
echo "Is 2 valid: " . json_encode($form->is_valid()) . "\n";
echo "Errors 2: " . json_encode($form->errors) . "\n";
echo "Cleaned 2 data: " . json_encode($form->cleaned_data) . "\n";
